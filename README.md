# NvForm

Small javascript lib to handle validation, masks and submition of Navve's forms in the browser.

## Dependencies

NvForm requires [validate.js](https://validatejs.org) and [imask.js](https://imask.js.org) to work.

## How to use

Use it by adding `data-*` attributes with the lib's directives in html elements. When initiated, NvForm will read the directives and take care of fields' validation and masks. It will also handle the submition to server.

### Init NvForm

NvForm must be attached to a form element to work properly. Do it by creating an instance of NvForm and calling the _init_ method.

````
const instance = new NvForm(
                           formElement, 
                           submitSuccessHandler, 
                           submitErrorHandler, 
                           extraDataToSend
);
instance.init();
````

The argument **extraDataToSend** is optional. Use it when you want to submit other data along with the form. That data will be appended into form's data.

### Directives

#### data-nv-validate-_validator-name_

Add to a form field element to make NvForm validate user's input.
_Validator name_ must be a valid name from *validate.js* lib. 

The directive value must be a json with validator's options. It's must have at least the `message` option.

##### data-nv-validate-checked

Besides the validators from *validate.js*, NvForm added one to check whether a checkbox is checked. Use it only on checkboxes or radio buttons.

##### data-nv-validate-equal

Besides the validators from *validate.js*, NvForm added one to check whether a value from a field is equal to another.
You must set the properties `selector` (css selector of the field to compare the value) and `message`.  

#### data-nv-validation-message

Add it to the element that must show the error message of a field. 

The directive value must be the field's name.

#### data-nv-mask

Use it to add a mask to a text input element. The mask must be a string valid for *imask.js*.

#### data-nv-validation-disabled-class

Add it to the element that you want to disable (tipically the submit button) when form is not valid.

The directive value should be the css class that must be added to the element when form is invalid.

#### data-nv-submitting-class

Use it to add a css class to an element when form is being submitted.

#### data-nv-form-parse-json

Add it in form element if you want NvForm to parse server's response as JSON.


## Submition url

NvForm will always submit the form using _POST_ method and to the url defined in form's **action** attribute.


## Author

This lib was coded by [Lanza](mailto:fabio.lanzarin@portotech.org) to make it easier to handle forms in Navve's sites. 