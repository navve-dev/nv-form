/**
 * Small lib to handle validation, masks and submition of Navve's forms in the browser.
 * @requires validate.js
 * @requires imask.js
 * @author Lanza
 */
(function (doc, root) {

    const factory = function (validate, imask, fetch) {

        /**
         * NvForm constructor.
         * @param formElement HTMLFormElement Form that NvForm must handle.
         * @param submitSuccessHandler Function Function that will handle server response on successful submition.
         * @param submitErrorHandler Function Function that will handle failed submition.
         * @param extraDataToSend Object Data to send along with form's data.
         * @constructor
         */
        const NvForm = function (formElement, submitSuccessHandler, submitErrorHandler, extraDataToSend) {
            this.el = formElement;
            this.valid = false;
            this.fields = [];
            this.submitting = false;
            this.successCallback = submitSuccessHandler || this.doNothing;
            this.errorCallback = submitErrorHandler || this.doNothing;
            this.extraData = extraDataToSend || null;
        };

        const p = NvForm.prototype;

        p.doNothing = function () {
        };

        p.setupCustomValidators = function () {
            /* Add custom validator in validate.js to see whether a checkbox button is checked. */
            validate.validators.checked = function (value, options, key, attributes) {
                if(!(options.checkboxElement && options.checkboxElement.checked)) {
                    return options.message || 'Você deve selecionar esta opção.';
                } else {
                    return null;
                }
            };

            /* Add custom validator in validate.js to check whether a value is equal to another one (to use in single call). */
            validate.validators.equal = function (value, options) {
                const fieldToCompare = options.selector ? doc.querySelector(options.selector) : null;
                if(fieldToCompare && 'value' in fieldToCompare) {
                    return value !== fieldToCompare.value ? options.message || 'Valor não é igual ao campo de conferência' : null;
                } else {
                    return null;
                }
            }
        };

        p.getValidatorDefinitions = function (element) {
            const attrRegex = /^data-nv-validate-([^\s]+)/;
            try {
                const defs = {};
                const replacable = /'/g;
                for(let b = 0, bn = element.attributes.length; b < bn; b++) {
                    let attr = element.attributes[b];
                    let matches = attr.name.match(attrRegex);
                    if(matches) {
                        let validatorName = matches[1];
                        let validatorOptions = JSON.parse(attr.value.replace(replacable, '"'));
                        if(validatorName === 'checked') {
                            validatorOptions.checkboxElement = element;
                        }
                        defs[validatorName] = validatorOptions;
                    }
                }
                return defs;
            } catch(err) {
                if(root.console && root.console.log) root.console.log(err);
                return {};
            }
        };

        p.getFields = function () {
            const elements =  this.el.querySelectorAll('input[name],select[name],textarea[name]');
            const ret = [];
            for(let a = 0, an = elements.length; a < an; a++) {
                let el = elements[a];
                ret.push({ element: el, validators: {}, valid: false, mask: null });
            }
            return ret;
        };

        p.getValidatableFields = function () {
            return this.fields.filter(field => Object.keys(field.validators).length > 0);
        };

        p.setupValidation = function () {
            this.setupCustomValidators();
            for(let a = 0, an = this.fields.length; a < an; a++) {
                let field = this.fields[a];
                let el = field.element;
                let validations = this.getValidatorDefinitions(el);
                if(Object.keys(validations).length) {
                    field.validators = validations;
                    if(el.type === 'checkbox' || el.type === 'radio') {
                        el.addEventListener('click', this.validateField.bind(this, field));
                    } else {
                        el.addEventListener('blur', this.validateField.bind(this, field));
                        el.addEventListener('change', this.resetField.bind(this, field));
                    }
                }
            }
            this.el.addEventListener('submit', this.submitForm.bind(this));
        };

        p.handleMaskFilled = function (field) {
            field.valid = true;
        };

        p.setupMasks = function () {
            for(let a = 0, an = this.fields.length; a < an; a++) {
                let field = this.fields[a];
                let maskValue = field.element.getAttribute('data-nv-mask');
                if(maskValue) {
                    field.mask = imask(field.element, { mask: maskValue });
                }
            }
        };

        p.setFieldMessage = function (field, message) {
            const messageEl = this.el.querySelector('[data-nv-validation-message="' + field.element.name + '"]');
            if(messageEl) {
                messageEl.innerText = message;
            }
        };

        p.updateSubmitting = function (value) {
            this.submitting = value;
            const els = this.el.querySelectorAll('[data-nv-submitting-class]');
            for(let a = 0, an = els.length; a < an; a++) {
                let el = els[a];
                let cssClass = el.getAttribute('data-nv-submitting-class');
                if(this.submitting) {
                    el.classList.add(cssClass);
                } else {
                    el.classList.remove(cssClass);
                }
            }
        };

        p.updateFormValid = function (value) {
            this.valid = value;
            const els = this.el.querySelectorAll('[data-nv-validation-disabled-class]');
            for(let a = 0, an = els.length; a < an; a++) {
                let el = els[a];
                let cssClass = el.getAttribute('data-nv-validation-disabled-class');
                if(this.valid) {
                    el.disabled = false;
                    el.classList.remove(cssClass);
                } else {
                    el.disabled = true;
                    el.classList.add(cssClass);
                }
            }
        };

        p.checkFormValid = function () {
            const fields = this.getValidatableFields();
            for(let a = 0, an = fields.length; a < an; a++) {
                let field = fields[a];
                if(!field.valid) {
                    this.updateFormValid(false);
                    return false;
                }
            }
            this.updateFormValid(true);
            return true;
        };

        p.getFieldValue = function (field) {
            const el = field.element;
            if(field.mask) {
                return field.mask.unmaskedValue;
            } else if(el.hasOwnProperty('selectedIndex')) {
                return el.options[el.selectedIndex].value;
            } else {
                return el.value;
            }
        };

        p.executeValidateSingle = function (field, value) {
            return validate.single(value, field.validators);
        };

        p.performFieldValidation = function (field, showError) {
            const value = this.getFieldValue(field);
            const validation = validate.single(value, field.validators);
            const fnFeedback = showError ? this.setFieldMessage : this.doNothing;
            if(validation && validation.length) {
                field.valid = false;
                fnFeedback.call(this, field, validation[0]);
            } else {
                field.valid = true;
                fnFeedback.call(this, field, '');
            }
        };

        p.validateField = function (field) {
            this.performFieldValidation(field, true);
            this.checkFormValid();
            return field.valid;
        };

        p.resetField = function (field) {
            this.setFieldMessage(field, '');
        };

        p.getFieldByName = function (fieldName) {
            for(let a = 0, an = this.fields.length; a < an; a++) {
                let field = this.fields[a];
                if(field.element.name === fieldName) {
                    return field;
                }
            }
            return null;
        };

        p.performFormValidation = function (showErrors) {
            const fields = this.getValidatableFields();
            for(let a = 0, an = fields.length; a < an; a++) {
                this.performFieldValidation(fields[a], showErrors);
            }
            this.checkFormValid();
        };

        p.validateForm = function () {
            this.performFormValidation(true);
            return this.valid;
        };

        p.submitForm = function (evt) {
            evt.preventDefault();
            if(!this.submitting && this.validateForm()) {
                this.updateSubmitting(true);
                const formData = new FormData(this.el);
                if(this.extraData) {
                    for(let prop in this.extraData) {
                        if(this.extraData.hasOwnProperty(prop)) {
                            formData.append(prop, this.extraData[prop]);
                        }
                    }
                }
                fetch(this.el.getAttribute('action') || '#', {
                    method: 'POST',
                    body: formData,
                    mode: 'cors'
                }).then(response => {
                    return response.ok ? Promise.resolve(response) : Promise.reject(response);
                }).then(response => {
                    const parseJson = this.el.getAttribute('data-nv-form-parse-json');
                    return parseJson === 'true' ? response.json() : Promise.resolve(response.body);
                }).then(responseBody => {
                    this.updateSubmitting(false);
                    this.successCallback.call(null, responseBody);
                }).catch(err => {
                    this.updateSubmitting(false);
                    this.errorCallback.call(null, err);
                });
            }
        };

        p.setupSubmit = function () {
            this.el.addEventListener('submit', this.submitForm.bind(this));
        };

        /**
         * Initiates NvForm. Sets up masks, validation and handles form's submition.
         */
        p.init = function () {
            this.fields = this.getFields();
            this.setupMasks();
            this.setupValidation();
            this.performFormValidation(false);
            this.setupSubmit();
        };

        return NvForm;

    };

    root.NvForm = factory(root.validate, root.IMask, root.fetch);

})(document, window);
